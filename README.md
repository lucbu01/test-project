# Test Project

## GitLab Pages für Branches:
 - [master](https://lucbu01.gitlab.io/test-project/master/)
 - [development](https://lucbu01.gitlab.io/test-project/development/)
 - [release](https://lucbu01.gitlab.io/test-project/release/)
 - [bugfix](https://lucbu01.gitlab.io/test-project/bugfix/)
 - [playground](https://lucbu01.gitlab.io/test-project/playground/)
 - [feature/BSK-167](https://lucbu01.gitlab.io/test-project/feature/BSK-167/)
